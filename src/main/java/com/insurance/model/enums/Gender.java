package com.insurance.model.enums;

public enum Gender {
	Male,
	Female,
	Other
}
