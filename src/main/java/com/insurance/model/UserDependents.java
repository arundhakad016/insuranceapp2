package com.insurance.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_dependents")
public class UserDependents {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private Date dob;
	
	@Column(name = "medical_condtion")
	private boolean medicalCondition;
	
	@Column(name = "medical_condition_desc")
	private String medicalConditionDesc;
	
	private String relation;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	

	public UserDependents() {
		super();
		// TODO Auto-generated constructor stub
	}



	public UserDependents(int id, String name, Date dob, boolean medicalCondition, String medicalConditionDesc,
			String relation, @NotNull User user) {
		super();
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.medicalCondition = medicalCondition;
		this.medicalConditionDesc = medicalConditionDesc;
		this.relation = relation;
		this.user = user;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Date getDob() {
		return dob;
	}



	public void setDob(Date dob) {
		this.dob = dob;
	}



	public boolean isMedicalCondition() {
		return medicalCondition;
	}



	public void setMedicalCondition(boolean medicalCondition) {
		this.medicalCondition = medicalCondition;
	}



	public String getMedicalConditionDesc() {
		return medicalConditionDesc;
	}



	public void setMedicalConditionDesc(String medicalConditionDesc) {
		this.medicalConditionDesc = medicalConditionDesc;
	}



	public String getRelation() {
		return relation;
	}



	public void setRelation(String relation) {
		this.relation = relation;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}

	
	
	
}
