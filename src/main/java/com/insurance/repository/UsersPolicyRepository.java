package com.insurance.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.insurance.model.User;
import com.insurance.model.UsersPolicy;
import com.insurance.model.enums.Status;

@Repository
public interface UsersPolicyRepository extends JpaRepository<UsersPolicy, Integer>{
	List<UsersPolicy> findByUser(User user);
	List<UsersPolicy> findByStatus(Status status);
}
