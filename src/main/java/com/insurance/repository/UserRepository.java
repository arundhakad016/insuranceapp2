package com.insurance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.insurance.model.Role;
import com.insurance.model.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	User findByEmail(String email);
	List<User> findByRole(Role roles);
	User findById(int id);
}
